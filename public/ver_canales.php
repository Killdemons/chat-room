<tr>
  <td><?=$canal->friendlyName?></td>
  <td>
  	<?php if (!isset($_SESSION['usuario']) || $_SESSION['usuario'] != 'Bryan'){
  	?>
    <div class="btn-group">
      <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Unirse
      </button>
      <div class="dropdown-menu">
        <form action="/public/chat_room.php">
          <p class="text-center">Nombre de usuario</p>
          <input type='text' class='form-control' id='user' name='user' placeholder=''>
          <input type="hidden" name="id" value="<?=$canal->sid?>">
          <input type='submit' class='btn btn-success' value='Unirme'>
        </form>
      </div>
    </div>
  	<?php
  	}else{
  	?>
  	<a class="btn btn-info btn-sm" href="/seguridad/canales.php?namemod=<?=$canal->friendlyName?>&id=<?=$canal->sid?>">Editar</a>
  	<a class="btn btn-danger btn-sm" href="/seguridad/delcanal.php?namemod=<?=$canal->friendlyName?>&id=<?=$canal->sid?>">Borrar</a>
  	<?php
  	}
  	?>
  </td>
</tr>
