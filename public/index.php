<?php  session_start();
include __DIR__."/../seguridad/Conexion.php";
?>
<!DOCTYPE html>
<html>
<head>
  <title>ChatRoom</title>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!-- Fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#">Chat Room</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="/../seguridad/login.php">Iniciar Sesion</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <?php
  if (filter_input(INPUT_GET, 'alert', FILTER_SANITIZE_STRING) == "true") {
  ?>
  <div class="alert alert-danger" role="alert">
    Nombre de usuario ya registrado
  </div>
  <?php
  }
  ?>
  <section>
    <div class="container">
      <h1 class="text-center">Bienvenido</h1>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="text-center">
        <h2>Elige un canal</h2>
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
             <thead>
              <tr>
                <th>Canales</th>
                <th>Opciones</th>
              </tr>
             </thead>
             <?php
             Ver_Servicio();
              $canales = Ver_Canal();
              if ($canales) {
                  foreach ($canales as $canal) {
                      require __DIR__ . '/ver_canales.php';
                  }
              }
              ?>
          </table>
        </div>
      </div>
    </div>
  </section>
</footer>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
