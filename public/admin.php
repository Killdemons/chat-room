<?php session_start();
include __DIR__."/../seguridad/Conexion.php";
require_once __DIR__.'/../shared/guard.php';
?>
<!DOCTYPE html>
<html>
<head>
  <title>Administrador</title>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!-- Fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
  <header>
    <?php require_once __DIR__."/../shared/navbar.php"; ?>
  </header>
  <section>
    <div>
      <h1 class="text-center">Bienvenido <?php print($_SESSION['usuario']);?></h1>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="text-center">
        <h2>Area Administrador</h2>               
        <h2> <?php Ver_Servicio()?> </h2>
        <div class="container">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Canales</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <?php
              $canales = Ver_Canal();
              if ($canales) {
                  foreach ($canales as $canal) {
                      require __DIR__ . '/ver_canales.php';
                  }
              }
              ?>
            </table>
            <div align="right">
              <a href="/seguridad/canales.php"><button type="button" class="btn btn-success btn-xs">Agregar Canal</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</footer>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
