<?php session_start();
include __DIR__."/../seguridad/Conexion.php";
$user = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
if (!isset($_SESSION['usuario'])) {
	$miembros = Read_Users_Canal($id);
	foreach ($miembros as $miembro) {
	    if ($miembro->identity == $user) {
	    	header("Location:/public/index.php?alert=true");
	    }
	}
	$_SESSION['usuario'] = Add_User_Canal($user, $id);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$mensaje = filter_input(INPUT_POST, 'mensaje', FILTER_SANITIZE_STRING);
    Send_Msjs_Canal($mensaje, $user, $id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Chat Room</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<!-- Fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/chat_room.css">
</head>
<body>
	<div class="container-fluid h-100">
		<div class="row justify-content-center h-100">
			<div class="col-md-4 col-xl-2 chat">
				<div class="card mb-sm-3 mb-md-0 contacts_card">
					<div class="card-header">
						<div class="user_info">
							<span>Usuarios conectados</span>
						</div>
					</div>
					<div class="card-body contacts_body" id="users">
						<ui class="contacts">
							<?php
							$members = Read_Users_Canal($id);
							foreach ($members as $member) {
							?>
							<li>
								<div class="d-flex bd-highlight">
									<div class="user_info">
										<i class="fas fa-user"></i>
							<?php
							echo "<span>". $member->identity ."</span>";
							?>
									</div>
								</div>
							</li>
							<?php
							}
							?>
						</ui>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
			<div class="col-md-8 col-xl-3 chat">
				<div class="card">
					<div class="card-header msg_head">
						<div class="d-flex bd-highlight">
							<div class="user_info">
								<span><?php echo "" . $user; ?></span>
							</div>
						</div>
						<div class="btn-group" id="action_menu_btn">
							<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-ellipsis-v"></i>
							</button>
							<div class="dropdown-menu dropdown-menu-right">
						    	<a class="dropdown-item" href="/seguridad/logout.php">Cerrar</a>
							</div>
						</div>
					</div>
					<div class="card-body msg_card_body" id="chat">
						<?php
						$messages = Read_Msjs_Canal($id);
						foreach ($messages as $message) {
							if ($message->from == $user) {
						?>
								<div class="d-flex justify-content-start mb-4">
									<div class="msg_cotainer">
										<?php echo "". $message->body; ?>
										<span class="msg_time">
										<?php echo "". $message->dateCreated->format('H:i:s d-m-Y') . " " . $message->from; ?>
										</span>
									</div>
								</div>
						<?php
							}else{
						?>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										<?php echo "". $message->body; ?>
										<span class="msg_time_send">
										<?php echo "". $message->dateCreated->format('H:i:s d-m-Y') . " " . $message->from; ?>
										</span>
									</div>
								</div>
						<?php
							}
						}
						?>
					</div>
					<div class="card-footer">
						<form method="POST">
							<div class="input-group">
								<textarea name="mensaje" class="form-control type_msg" placeholder="Escriba su mensaje..."></textarea>
								<div class="input-group-append">
									<button type='submit' class='input-group-text send_btn'><i class='fas fa-location-arrow'></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/assets/js/chat-box.js"></script>
	<script type="text/javascript" src="/assets/js/users-box.js"></script>
</body>
</html>