<?php
require __DIR__.'/../APITwilio/Twilio/autoload.php';
require __DIR__.'/../APITwilio/vendor/autoload.php';
use Twilio\Rest\Client;

$_SESSION['sid'] = "AC08ba4cb1f38d170b2f55148eb922f7da";
$_SESSION['token'] = "c783d1af8f6db9e3be9da2892379a514";

//Crear Servicio
function Crear_Servicio(){
        $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
        $service = $twilio->chat->v2->services
                                    ->create("Chat Room");
        $_SESSION['servicesid'] = $service->sid;
        return $service->friendlyName;
}

//Vista de los servicios creados
function Ver_Servicio(){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $services = $twilio->chat->v2->services
                             ->read();
    if (count($services)==0) {
        print(Crear_Servicio());
    }else{
        foreach ($services as $service) {
            $_SESSION['servicesid'] = $service->sid;
            print($service->friendlyName);
        }
    }

}

//Vista de los canales creados
function Ver_Canal(){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $channels = $twilio->chat->v2->services($_SESSION['servicesid'])
                                 ->channels
                                 ->read();
    return $channels;
}   

//Crear un canal
function Crear_Canal($nombre){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $channel = $twilio->chat->v2->services($_SESSION['servicesid'])
                                ->channels
                                ->create(array("friendlyName" => $nombre));
}

//Modificar un Canal
function Modificar_Canal($nombre, $id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $channel = $twilio->chat->v2->services($_SESSION['servicesid'])
                                ->channels($id)
                                ->update(array("friendlyName" => $nombre));
}

//Eliminar un Canal
function Eliminar_Canal($id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $twilio->chat->v2->services($_SESSION['servicesid'])
                     ->channels($id)
                     ->delete();
}

//Unirse a un Canal
function Add_User_Canal($nombre, $id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $member = $twilio->chat->v2->services($_SESSION['servicesid'])
                                ->channels($id)
                                ->members
                                ->create($nombre);
    return $member->sid;
}

//Leer miembros de un canal
function Read_Users_Canal($id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $members = $twilio->chat->v2->services($_SESSION['servicesid'])
                                ->channels($id)
                                ->members
                                ->read();
    return $members;
}

//Eliminar miembros de un canal
function Del_Users_Canal($identity, $id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $members = $twilio->chat->v2->services($_SESSION['servicesid'])
                                ->channels($id)
                                ->members($identity)
                                ->delete();
}

//Cargar Mensajes de un canal
function Read_Msjs_Canal($id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $messages = $twilio->chat->v2->services($_SESSION['servicesid'])
                                 ->channels($id)
                                 ->messages
                                 ->read();
    return $messages;
}

//Enviar Mensajes de un canal
function Send_Msjs_Canal($mensaje, $user, $id){
    $twilio = new Client($_SESSION['sid'], $_SESSION['token']);
    $message = $twilio->chat->v2->services($_SESSION['servicesid'])
                            ->channels($id)
                            ->messages
                            ->create(array("body" => $mensaje, "from"  => $user));
}