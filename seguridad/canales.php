<?php
session_start();
include __DIR__."/../seguridad/Conexion.php";
require_once __DIR__.'/../shared/guard.php';

$nombre = filter_input(INPUT_GET, 'namemod', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $name = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
  if (!isset($id) || $id == null || $id == "") {
    Crear_Canal($name);
    return header('Location: /public/admin.php');
  }else{
    Modificar_Canal($name, $id);
    return header('Location: /public/admin.php');
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Nuevo Canal</title>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!-- Fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
  <header>
    <?php require_once __DIR__."/../shared/navbar.php"; ?>
  </header>
  <section>
    <div class="container">
      <div class="text-center">
        <h2>Nuevo Canal</h2>               
        <div class="container">
          <form method="POST">
            <div class="form-group">
              <label for="nombre"><i class="fas fa-user"></i> Nombre</label>
              <input type="nombre" class="form-control" id="nombre" aria-describedby="usernameHelp" placeholder="Nombre" name="nombre" value="<?=$nombre ?? ''?>">
            </div>
            <?php
            if (!isset($id) || $id == null || $id == "") {
            ?>
              <button class="btn btn-dark float-right" type="submit">Crear <i class="fas fa-plus-circle"></i></i></button>
            <?php
            }else {
            ?>
              <button class="btn btn-dark float-right" type="submit">Editar <i class="fas fa-plus-circle"></i></i></button>
            <?php
            }
            ?>
          </form>
        </div>
      </div>
    </div>
  </section>
</footer>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>